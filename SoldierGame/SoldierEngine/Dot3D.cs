﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoldierEngine
{
    internal class Dot3D : Dot
    {
        protected int _z;

        public Dot3D(int x, int y, int z, char sign) : base(x, y, sign)
        {
            this._z = z;
        }

        public int Z { get { return _z; } set { _z = value; } }
    }
}
