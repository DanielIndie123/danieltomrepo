﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoldierEngine
{
    public class InputHandler
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern short GetKeyState(int key);

        private Dictionary<WindowsVirtualKey, bool> _pressedCurrentFrame;
        private Dictionary<WindowsVirtualKey, bool> _pressedPrevFrame;

        public InputHandler()
        {
            _pressedCurrentFrame = new Dictionary<WindowsVirtualKey, bool>();
            _pressedPrevFrame = new Dictionary<WindowsVirtualKey, bool>();
        }

        public void Update()
        {
            foreach (WindowsVirtualKey key in Enum.GetValues(typeof(WindowsVirtualKey)))
            {
                _pressedPrevFrame[key] = _pressedCurrentFrame.ContainsKey(key) && _pressedCurrentFrame[key];
                _pressedCurrentFrame[key] = (GetKeyState((int)key) & 0x8000) != 0;
            }
        }

        public bool IsKeyDown(WindowsVirtualKey key)
        {
            return _pressedCurrentFrame[key];
        }

        public bool IsKeyPressed(WindowsVirtualKey key)
        {
            return _pressedCurrentFrame[key] && !_pressedPrevFrame[key];
        }

        public bool IsKeyReleased(WindowsVirtualKey key)
        {
            return !_pressedCurrentFrame[key] && _pressedPrevFrame[key];
        }
    }
}
