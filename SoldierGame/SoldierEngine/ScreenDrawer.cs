﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SoldierEngine
{
    internal class ScreenDrawer
    {
        private readonly int _width;
        private readonly int _height;
        private DotContainer[,] _dotsMatrix;
        
        public ScreenDrawer(int width, int height)
        {
            this._width = width;
            this._height = height;
            SetEmptyMatrix();
        }

        public void Draw(List<Dot3D> dots)
        {
            Console.Clear();
            SetEmptyMatrix();
            AddDotsToMatrix(dots);
            DrawMatrix();
        }

        private void DrawMatrix()
        {
            string buffer = "";
            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width; x++)
                {
                    buffer += _dotsMatrix[x, y].GetTop().Sign;
                }
                buffer += Environment.NewLine;
            }
            Console.Write(buffer);
        }

        private void AddDotsToMatrix(List<Dot3D> dots)
        {
            foreach (var dot in dots)
            {
                if(IsDotInMatrix(dot.X, dot.Y))
                    _dotsMatrix[dot.X, dot.Y].Add(dot);
            }
        }

        private bool IsDotInMatrix(int x, int y)
        {
            return x >= 0 && x < _width && y >= 0 && y < _height;
        }

        private void SetEmptyMatrix()
        {
            _dotsMatrix = new DotContainer[_width, _height];
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    _dotsMatrix[x, y] = new DotContainer(x, y);
                }
            }
        }
    }
}