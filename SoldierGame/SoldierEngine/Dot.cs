﻿using System;

namespace SoldierEngine
{
    public class Dot
    {
        protected int _x;
        protected int _y;
        
        protected char _sign;

        public Dot(int x, int y, char sign)
        {
            this._x = x;
            this._y = y;
            
            this._sign = sign;
        }

        public int X { get { return _x; } set { _x = value; } }
        public int Y { get { return _y; } set { _y = value; } }
        public char Sign
        {
            get { return _sign; }
            set { _sign = value; }
        }
    }
}