﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoldierEngine
{
    public class Graphics
    {
        private List<Dot> _dotsGraphics;

        public static Graphics TemplateGraphics
        {
            get
            {
                return new Graphics(new List<Dot>() { new Dot(0, 0, '#') });
            }
        }

        public Graphics(List<Dot> graphics)
        {
            _dotsGraphics = graphics;
        }

        public Graphics(char singleSignGraphics)
        {
            _dotsGraphics = new List<Dot>() {new Dot(0,0,singleSignGraphics)};
        }

        public List<Dot> GetGraphic()
        {
            return _dotsGraphics;
        } 
    }
}
