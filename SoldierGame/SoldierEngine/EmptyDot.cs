using System;

namespace SoldierEngine
{
    internal class EmptyDot : Dot3D
    {
        public EmptyDot(int x, int y) : base(x, y, -1, ' ')
        {

        }
    }
}