﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoldierEngine
{
    public class Entity
    {
        public Graphics Graphics { get; private set; }
        private int _x;
        private int _y;
        private int _z;

        public int X
        {
            get { return _x; } set { _x = value; }
        }
        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public int Z
        {
            get { return _z; }
            set { _z = value; }
        }

        public Entity(int x, int y, int layer, Graphics graphics = null)
        {
            X = x;
            Y = y;
            Graphics = graphics;
            Z = layer;
        }

        public virtual void Update()
        {
            
        }
    }
}
