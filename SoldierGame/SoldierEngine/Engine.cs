﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SoldierEngine
{
    public class Engine
    {
        private List<Entity> _entities;
        private List<Graphics> _graphics;
        private ScreenDrawer _screenDrawer;
        private InputHandler _inputHandler;

        public InputHandler InputHandler
        {
            get { return _inputHandler;}
        }

        public Engine(int width, int height)
        {
            FramesPerSecond = 60;
            _entities = new List<Entity>();
            _inputHandler = new InputHandler();
            _graphics = new List<Graphics>();
            _screenDrawer = new ScreenDrawer(width, height);
        }

        public int FramesPerSecond { get; set; }

        public void AddGraphic(Graphics graphics)
        {
            _graphics.Add((graphics));
        }

        public void RemoveGraphics(Graphics graphics)
        {
            _graphics.Remove(graphics);
        }

        public void AddEntity(Entity entity)
        {
            _entities.Add(entity);
        }

        public void RemoveEntity(Entity entity)
        {
            _entities.Remove(entity);
        }

        public void Update()
        {
            _inputHandler.Update();
            UpdateEntities();
        }

        public void Draw()
        {
            DrawEntities();
        }

        private void DrawEntities()
        {
            List<Dot3D> pixelsInScreen = new List<Dot3D>();

            foreach (var entity in _entities)
            {
                Graphics entityGraphics = entity.Graphics;
                if (entityGraphics != null)
                {
                    foreach (var pixels in entityGraphics.GetGraphic())
                    {
                        pixelsInScreen.Add(new Dot3D(entity.X + pixels.X, entity.Y + pixels.Y, entity.Z, pixels.Sign));
                    }
                }
            }

            _screenDrawer.Draw(pixelsInScreen);
        }

        private void UpdateEntities()
        {
            foreach (var entity in _entities)
            {
                entity.Update();
            }
        }

        public virtual void OnFrame()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            do
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                Update();
                Draw();
                OnFrame();

                FpsLock((int)stopwatch.ElapsedMilliseconds);
            } while (!InputHandler.IsKeyReleased(WindowsVirtualKey.VK_ESCAPE));
        }

        private void FpsLock(int elsapedmiliseconds)
        {
            int stepMiliseconds = 1000 / FramesPerSecond - elsapedmiliseconds;
            if (stepMiliseconds < 0)
            {
                return;
            }
            
            Thread.Sleep(stepMiliseconds);
        }
    }
}
