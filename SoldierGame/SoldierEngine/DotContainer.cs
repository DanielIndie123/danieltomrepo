﻿using System.Collections.Generic;
using System.Linq;

namespace SoldierEngine
{
    internal class DotContainer
    {
        private List<Dot3D> _dots;
        private int _x;
        private int _y;

        public DotContainer(int x, int y)
        {
            _dots = new List<Dot3D>();
            this._x = x;
            this._y = y;
            _dots.Add(new EmptyDot(x,y) );
        }

        public void Add(Dot3D dot)
        {
            _dots.Add(dot);
        }


        public Dot3D GetTop()
        {
            if (_dots.Count == 0)
            {
                return null;
            }
            return _dots.OrderByDescending(dot => dot.Z).First();
        }
    }
}