﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoldierEngine;

namespace GameMechanic
{
    public class Square: Shape
    {
        public Square(int size, char squareSign): base(CreateDots(size, squareSign))
        {
            
        }

        private static List<Dot> CreateDots(int size, char squareSign)
        {
            List<Dot> squareDots = new List<Dot>();
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    squareDots.Add(new Dot(x, y, squareSign));
                }
            }
            return squareDots;
        }
    }
}
