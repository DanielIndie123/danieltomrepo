﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SoldierEngine;

namespace GameMechanic
{
    class Program
    {
        public static MyGame engine;
        static void Main(string[] args)
        {
            engine = new MyGame(20, 20);
            engine.Run();
        }
    }

    public class MyGame : Engine
    {
        private Entity myEntity;

        public MyGame(int width, int height) : base(width, height)
        {
            myEntity = new EntityExample(5, 5, 0, new Square(3, '*'));
            AddEntity(myEntity);
        }

        public override void OnFrame()
        {
        }
    }

    public class EntityExample : Entity
    {
        public override void Update()
        {
            X += (Program.engine.InputHandler.IsKeyDown(WindowsVirtualKey.VK_RIGHT) ? 1 : 0) - (Program.engine.InputHandler.IsKeyDown(WindowsVirtualKey.VK_LEFT) ? 1 : 0);
            Y += (Program.engine.InputHandler.IsKeyDown(WindowsVirtualKey.VK_DOWN) ? 1 : 0) - (Program.engine.InputHandler.IsKeyDown(WindowsVirtualKey.VK_UP) ? 1 : 0);
        }

        public EntityExample(int x, int y, int layer, Graphics graphics = null) : base(x, y, layer, graphics)
        {
            
        }
    }
}
